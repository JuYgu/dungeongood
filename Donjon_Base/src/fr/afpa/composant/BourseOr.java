package fr.afpa.composant;

public class BourseOr extends ItemGame implements Item {
	
	private int or;
	
	public BourseOr(int posX, int posY) {
		super(posX, posY);
		or = Integer.parseInt(rb.getString("item.or"));
	}

	public BourseOr() {
		or = Integer.parseInt(rb.getString("item.or"));
	}
	
	public int getOr() {
		return or;
	}

	public void setOr(int or) {
		this.or = or;
	}

}
