package fr.afpa.composant;

public class ItemGame {
	private int posX;
	private int posY;
	private int id;
	
	static int count;
	
	public ItemGame(int posX, int posY) {
		this.id = ++count;
		this.posX = posX;
		this.posY = posY;
	}
	
	public ItemGame() {
	}



	public int getPosX() {
		return posX;
	}
	public void setPosX(int posX) {
		this.posX = posX;
	}
	public int getPosY() {
		return posY;
	}
	public void setPosY(int posY) {
		this.posY = posY;
	}
	public int getId() {
		return id;
	}




}
