package fr.afpa.properties;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

public class Props {


		// Pour r�cup�rer les information d'un fichier .properties
		ResourceBundle rb = ResourceBundle.getBundle("fr.afpa.properties.config");
		String env = rb.getString("env");	
		// Pour r�cuperer la valeur d'une variable d'environnement 
		String javaHomeValue = System.getenv().get(env);

}
