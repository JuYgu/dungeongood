package fr.afpa.composant;

public class PotionVie extends ItemGame implements Item {


	int pointDeVie;
	
	public PotionVie(int posX, int posY) {
		super(posX, posY);
		pointDeVie = Integer.parseInt(rb.getString("item.pdv"));
	}

	public PotionVie() {
		pointDeVie = Integer.parseInt(rb.getString("item.pdv"));
	}

	public int getPointDeVie() {
		return pointDeVie;
	}
	public void setPointDeVie(int pointDeVie) {
		this.pointDeVie = pointDeVie;
	}


}
