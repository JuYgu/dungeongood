package fr.afpa.user;

import java.io.Serializable;
import java.time.Instant;
import java.util.Comparator;

import fr.afpa.composant.Personnage;

public class User implements Serializable, Comparable<User>,Comparator<User>{
	private String nom;
	private String prenom;
	private int or;
	private Instant dateJeu;
	
	
	public User(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public int getOr() {
		return or;
	}


	public void setOr(int or) {
		this.or = or;
	}


	public Instant getDateJeu() {
		return dateJeu;
	}


	public void setDateJeu(Instant date) {
		this.dateJeu = date;
	}
	
	

	public int compareTo(User joueur) {
		
		 return (this.or - joueur.or );
	}
	
	public int compare (User a, User b) {
		return a.getOr() - b.getOr();
		}
	
	
	
	
	
	
}


