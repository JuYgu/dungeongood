package fr.afpa.composant;

import java.io.Serializable;
import java.util.ResourceBundle;

import fr.afpa.utils.Utils;

public class Joueur extends Personnage implements Serializable{


	private  int posX;
	private  int posY;

	public Joueur (String nom, int pointDeVie, int pointDeForce, int or) {
		super(nom, pointDeVie, pointDeForce, or);
	}
	
	public Joueur (String nom) {

		super(nom, Integer.parseInt(rb.getString("joueur.pdv")), Integer.parseInt(rb.getString("joueur.pdf")), Integer.parseInt(rb.getString("joueur.or")));
		this.posX = Integer.parseInt(rb.getString("joueur.x"));
		this.posY = Integer.parseInt(rb.getString("joueur.y"));
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}
	
	public int getPointDeVie() {
		return pointDeVie;
	}

	public void setPointDeVie(int pointDeVie) {
		this.pointDeVie = pointDeVie;
	}

	public int getPointDeForce() {
		return pointDeForce;
	}

	public void setPointDeForce(int pointDeForce) {
		this.pointDeForce = pointDeForce;
	}

	public int getOr() {
		return or;
	}

	public void setOr(int or) {
		this.or = or;
	}
	
	
	public void getPositions() {
		System.out.println("x : "+posX+", y :"+posY);
	}
	
	
	
	// M�thodes d'action -------------------------------------------------------------
	

	@Override
	public void fight(Personnage monstre) {
		monstre.setPointDeVie(monstre.getPointDeVie()-getPointDeForce());
	}




	@Override
	public String toString() {
		return "Joueur [pointDeVie=" + pointDeVie + ", pointDeForce=" + pointDeForce + ", or=" + or + ", getNom()="
				+ getNom() + "]";
	}

	




	










}
