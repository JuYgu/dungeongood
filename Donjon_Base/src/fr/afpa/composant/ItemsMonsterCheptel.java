package fr.afpa.composant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.Map.Entry;

import fr.afpa.plateau.Donjon;
import fr.afpa.plateau.Salle;
import fr.afpa.utils.Utils;

public class ItemsMonsterCheptel implements Utils{
	List <Monstre>monsterCheptel;
	List <ItemGame>itemsCheptel;
	private int compteur;
	private int compteurCleverMonster;
	int nbMonstre;
	int nbItem;
	
	
	public ItemsMonsterCheptel (int nbItem, int nbMonstre, int x, int y) {
		Personnage.defaultName = new String[] {"Jojo", "Maurice", "Jean-Charles", "Sympatoche", "Michel", "Pierre-Edouard", "Gonzague"};
		this.nbMonstre = nbMonstre;
		this.nbItem = nbItem;
		monsterCheptel = new ArrayList<>();
		itemsCheptel = new ArrayList<>();
	//	 On int�gre le nombre voulu d'item dans la list itemsCheptel
		for (int i = 0; i < nbItem/4; i++) {
			itemsCheptel.add(new BourseOr(randInt(1, x-1), randInt(1, y-1)));
			itemsCheptel.add(new BanditManchot(randInt(1, x-1), randInt(1, y-1)));
			itemsCheptel.add(new PotionForce(randInt(1, x-1), randInt(1, y-1)));
			itemsCheptel.add(new PotionVie(randInt(1, x-1), randInt(1, y-1)));
		}
		for (int i = 0; i < this.nbMonstre/2; i++ ) {
			monsterCheptel.add(new CleverMonster(randInt(1, x-1), randInt(1, y-1)));
			monsterCheptel.add(new SillyMonster(randInt(1, x-1), randInt(1, y-1)));
		}
	}
		
	public List<Monstre> getMonsterCheptel() {
		return monsterCheptel;
	}
	public void setMonsterCheptel(List<Monstre> monsterCheptel) {
		this.monsterCheptel = monsterCheptel;
	}

	public List<ItemGame> getItemsCheptel() {
		return itemsCheptel;
	}
	public void setItemsCheptel(List<ItemGame> itemsCheptel) {
		this.itemsCheptel = itemsCheptel;
	}


	public int getCompteur() {
		return compteur;
	}

	public void setCompteur(int compteur) {
		this.compteur = compteur;
	}

	public int getCompteurCleverMonster() {
		return compteurCleverMonster;
	}

	public void setCompteurCleverMonster(int compteurCleverMonster) {
		this.compteurCleverMonster = compteurCleverMonster;
	}
	
	
// METHODE DE RECUPERATION DES TYPE ET ID D'OBJET ET MONSTRE
	// M�thode permettant de r�cup�rer le type de l'objet
	 public String getItemType(int x, int y) {
		 String itemType = "";
		 int compteur = 0;
			for (ItemGame item : getItemsCheptel()) {
				if(item.getPosX() == x && item.getPosY() == y) {
					if(compteur >= 1) {
						itemType +=", "+item.getClass().getSimpleName();
						compteur++;
					} else {
						itemType += item.getClass().getSimpleName()+"";
						compteur++;
					}
				}
			}
			return itemType;
	 }
	 
		// M�thode permettant de r�cup�rer le type de l'objet
	 public String getItemId(int x, int y) {
		 String itemType = "";
		 int compteur = 0;
			for (ItemGame item : getItemsCheptel()) {
				if(item.getPosX() == x && item.getPosY() == y) {
					if(compteur >= 1) {
						itemType +="/"+item.getId()+"";
						compteur++;
					} else {
						itemType += item.getId()+"";
						compteur++;
					}
				}
			}
			return itemType;
	 }
	 
	 public String getMonsterId(int x, int y) {
		 String itemType = "";
		 int compteur = 0;
			for (Monstre monstre : monsterCheptel) {
				if(monstre.getPosX() == x && monstre.getPosY() == y) {
					if(compteur >= 1) {
						itemType +="/"+monstre.getId()+"";
						compteur++;
					} else {
						itemType += monstre.getId()+"";
						compteur++;
					}
				}
			}
			return itemType;
	 }
	 
	 
	 // METHODE SERVANT AU MOUVEMENT DES MONSTRES ET A TROUVER LE PLUS LONG CHEMIN ENTRE LE GAMER ET LE CLEVER MONSTER----------------------------------------------------------------------------------------------------------------------
	 
	 public void moveSillyMonsters(Donjon donjon, Personnage joueur) {
		 for (int i = 0; i < getMonsterCheptel().size(); i++) {
			 if(getMonsterCheptel().get(i) instanceof SillyMonster) {
				 getMonsterCheptel().get(i).moveMonster(getMonsterCheptel().get(i), donjon);
			 }
		 }
	 }
	 
	 
	 public void moveCleverMonster(Donjon donjon, Personnage joueur) {
		 int nbCleverMonster = 0;
		 if(!getMonsterCheptel().isEmpty()) {
			 for (int i = 0; i < getMonsterCheptel().size(); i++) {
				 if(getMonsterCheptel().get(i) instanceof CleverMonster) {
					 if (getCompteurCleverMonster() == 0) {
						 getMonsterCheptel().get(i).moveMonster(getMonsterCheptel().get(i), donjon);
					 } else {
						 int move =  getPathToTheGamer(getMonsterCheptel().get(i), donjon, joueur);
						 donjon.resetVisited();
						 for(int c = 0; c < donjon.getX(); c++) {
							 for (int j = 0; j < donjon.getY(); j++) {
								 if(donjon.getDonjon()[c][j].getId() == move) {
									 getMonsterCheptel().get(i).setPosX(donjon.getDonjon()[c][j].getPositionX());
									 getMonsterCheptel().get(i).setPosY(donjon.getDonjon()[c][j].getPositionY());
									 nbCleverMonster++;
									 j = donjon.getY();
									 c = donjon.getX();
								 }
							 }
						 }
					 }
					
				 }
					
			 }
		 }

		 if (getCompteurCleverMonster() == 1) {
			 if(nbCleverMonster == 1) {
				 System.out.println("Un monstre a pu sentir votre odeur et s'avance vers vous !");

			 } else {
				 System.out.println(nbCleverMonster+" monstres ont pu sentir votre odeur et s'avancent vers vous !");
			 }
			 setCompteurCleverMonster(getCompteurCleverMonster()-1);
		 }
		 
		 
	 }
	 
	 
	 public int getPathToTheGamer( Monstre cleverMonster, Donjon donjon, Personnage joueur){

		 // On r�cup�re l'id de la chambre o� se trouve le monstre et le joueur
		 int indice = getRoomId(joueur, donjon);
		 int idDirection = 0;

		 int x = joueur.getPosX();
		 int y = joueur.getPosY();
		 
		 int xMonster = cleverMonster.getPosX();
		 int yMonster = cleverMonster.getPosY();

		 
		LinkedList<Salle> listSalles = new LinkedList<>();
		Map <Integer, Integer> nodes = new HashMap<>();
		
		listSalles.push(donjon.getDonjon()[x][y]);
		indice++;
		while((indice-1 != donjon.getDonjon()[xMonster][yMonster].getId())) {
			do {
				//down
				if(y + 1 < donjon.getY()  && donjon.getDonjon()[x][y+1].isVisited() == false && donjon.getDonjon()[x][y+1].getId() == indice ) {
					listSalles.push(donjon.getDonjon()[x][y+1]);
					donjon.getDonjon()[x][y+1].setVisited(true);
					y++;
					indice++;

				} 
				// right
				else if (x + 1 < donjon.getX()  && donjon.getDonjon()[x+1][y].isVisited() == false  && donjon.getDonjon()[x+1][y].getId() == indice) {
					x++;
					listSalles.push(donjon.getDonjon()[x][y]);
					donjon.getDonjon()[x][y].setVisited(true);
					indice++;

				}
				// up
				else if (y - 1 >= 0  && donjon.getDonjon()[x][y-1].isVisited() == false  && donjon.getDonjon()[x][y-1].getId() == indice) {
					y--;
					listSalles.push(donjon.getDonjon()[x][y]);
					donjon.getDonjon()[x][y].setVisited(true);
					indice++;

				}		
				// left
				else if (x - 1 >= 0  && donjon.getDonjon()[x-1][y].isVisited() == false  && donjon.getDonjon()[x-1][y].getId() == indice) {
					x--;
					listSalles.push(donjon.getDonjon()[x][y]);
					donjon.getDonjon()[x][y].setVisited(true);
					indice++;

					} else {
						break;
				}
				
			} while (donjon.getDonjon()[xMonster][yMonster].getId() != indice-1);
			
				if (listSalles.size() != 0 ) {
					listSalles.removeFirst();
					x =listSalles.getFirst().getPositionX();
					y = listSalles.getFirst().getPositionY();
				}	
		}
		
		idDirection = listSalles.getFirst().getId();
		return idDirection;
	}
		
	 
	 //-METHODE SERVANT A L'UTILISATION DES OBJETS ET DES ITEMS
	 public void pickItemFightMonster(Personnage joueur) {
			int compteurItem = checkItem(joueur);
			int compteurMonster = checkMonster(joueur);
			// R�cup�ration des infos des items
			String [] itemsId = getItemId(joueur.getPosX(), joueur.getPosY()).split("/");
			String [] itemsType = getItemType(joueur.getPosX(), joueur.getPosY()).split(",");
			
			// R�cup�ration des infos des monstres
			String [] monsterId = getMonsterId(joueur.getPosX(), joueur.getPosY()).split("/");
			
			Scanner in = new Scanner(System.in);
			
			//D�but des choix
			if (compteurItem > 0 && compteurMonster>0 ) {			
				String choix = "";
				System.out.println("Il y a "+compteurItem+" objet(s) ("+getItemType(joueur.getPosX(), joueur.getPosY()) +")  et "+compteurMonster+" monstre(s) dans la pi�ce !");
				
				System.out.println("Vous allez �tre oblig� d'affronter "+compteurMonster+" monstre(s) !");
				System.out.println("Souhaitez-vous utiliser l'objet avant de combattre ? ( O ou n'importe quel autre caract�re )");
				choix = in.next();
				System.out.println(choix);
				if ("o".equalsIgnoreCase(choix)) {
					if ( compteurItem >= 1) {
						System.out.println("Vous avez le choix entre :");
						for ( int i = 0; i < itemsType.length; i++) {
							System.out.println((i+1)+"- "+itemsType[i]);
						}
						int choice = (in.nextInt()-1);
						joueur.useItem(Integer.parseInt(itemsId[choice]), getItemsCheptel());
					} 
				}
					
				fight(compteurMonster, monsterId, joueur);
				
					
					
			} else if (compteurItem > 0 || compteurMonster>0 ) {
				if (compteurItem > 0) {
					System.out.println("Il y a "+compteurItem+" objet(s) ("+getItemType(joueur.getPosX(), joueur.getPosY()) +")  dans la pi�ce !");
					System.out.println("Souhaitez vous en utiliser un avant de vous d�placer ? ( O pour oui, n'importe quel autre caractere pour non)");
					String choix = in.next();

					if ("O".equalsIgnoreCase(choix)) {
						if ( compteurItem > 1) {
							System.out.println("Vous avez le choix entre :");
							for ( int i = 0; i < itemsType.length; i++) {
								System.out.println((i+1)+"- "+itemsType[i]);
							}
							int choice = in.nextInt()-1;
							joueur.useItem(Integer.parseInt(itemsId[choice]), itemsCheptel);
						} else {
							joueur.useItem(Integer.parseInt(itemsId[0]), itemsCheptel);
						}
					}
				} else if (compteurMonster > 0) {
					System.out.println("\n---------------------------------DOUM--DOUM--DOUM-------------------------------------");
					System.out.println("\nA peine arriv� dans la pi�ce, vous vous rendez compte que vous allez maintenant devoir affronter "+compteurMonster+ " monstre(s)"
							+ "\nDieu vous garde !");
					fight(compteurMonster, monsterId, joueur);
				}
				
			}
		}
		
	 
	 public void fight(int compteurMonster, String [] monsterId, Personnage joueur) {
			String indice = "";
			String nom ="";
			System.out.println("----------------------------------------------*** ! ! COMBAT ! ! ***-----------------------------------------------");
			 for (int i = 0; i < monsterId.length; i++) {
				 for (Monstre monstre : getMonsterCheptel()) {
					 if(monstre.getId() == Integer.parseInt(monsterId[i])) {
						 nom = monstre.getNom();
						 System.out.println(monstre.toString());
						 Utils.sleep();
						 Utils.sleep();
						 do {
							 ((Joueur)joueur).fight(monstre); 
							 System.out.println("\nVous attaquez le monstre !");
							 Utils.sleep();
							 System.out.println("Points de vie du monstre "+monstre.getPointDeVie());
							 if (monstre.getPointDeVie() > 0) {
								 System.out.println("\nLe monstre vous attaque !");
								 monstre.fight(joueur);
								 Utils.sleep();
								 System.out.println("Vos points de vie apr�s cette attaque : "+joueur.getPointDeVie());
							 }

							 
						 } while (joueur.getPointDeVie() > 0 && monstre.getPointDeVie() >0);
						 if(joueur.getPointDeVie() <= 0) {
							 Utils.sleep();
							 System.out.println(" \n*****************************************************************");
								System.out.println("*                       DOUM DOOUM DOOOUUM                      *");

							 System.out.println("\nVous vous �tes battu en h�ros mais vous n'avez pas surv�cu. \nLe service du donjon s'occupera d'envoyer quelques deniers � votre veuve");
							
							 System.out.println(" \n****************************************************************");
								System.out.println("*                   -----> FIN DU JEU <-----                   *");
								System.out.println("****************************************************************\n\n");
							 i = monsterId.length;
							 compteurMonster = 0;
							 break;
						 } else if(monstre.getPointDeVie() <= 0) {
							 System.out.println("\n***** !!! �o_ PIN PIN PIN PIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIN _o�  !!! *****");
							 Utils.sleep(); 
							 System.out.println("\nBravo ! Vous avez vaincu l'affreux "+nom+". Vous gagnez tout l'or qu'il possedait");
							 joueur.setOr(joueur.getOr()+ monstre.getOr());
							 Utils.sleep();
							 System.out.println("+ "+monstre.getOr()+ " pi�ces d'or");
							 System.out.println("MAIS mais ! je n'en crois pas mes yeux ! Vos muscles croissent de mani�re impressionnante !");
							 int pdf =  ItemsMonsterCheptel.randInt(1, 3);
							 joueur.setPointDeForce(joueur.getPointDeForce()+ pdf);
							 System.out.println("+ "+pdf+" points de force");
							 System.out.println();
							 Utils.sleep();
							 
							 indice = getMonsterCheptel().indexOf(monstre)+ "/";
							 compteurMonster--;
						 }
		
					 }
				 }
				 if(compteurMonster > 0) {
					 System.out.println("Vous devez malheureusement affronter un autre monstre. Celui-ci est d'autant plus �nerv� que vous avez achev� "+nom+", son copain d'enfance. \n"+randInt(3, 15)+" ans qu'ils jouaient ensemble aux cartes dans la m�me pi�ce, tu penses bien que �a cr��e des liens.");
					 Utils.sleep();
					 Utils.sleep();
				 }
			 }
			 if(joueur.getPointDeVie()> 0) {
				 String [] monstres = indice.split("/");
				 for(int i = 0; i < monstres.length; i++) {
					 getMonsterCheptel().remove(Integer.parseInt(monstres[i])); 
				 }
				
			 }
		 }
	 
	 
		//Retourne le nombre de monstres se trouvent sur la pi�ce
		public int checkMonster(Personnage joueur) {
			boolean isMonster = false;
			int compteurMonster = 0;
			for (Monstre monster : getMonsterCheptel()) {
				if(monster.getPosX() == joueur.getPosX() && monster.getPosY() == joueur.getPosY()) {
					compteurMonster++;
				}
			}
			return compteurMonster;
		}
		
		//Retourne le nombre d'items se trouvent sur la pi�ce
		public int checkItem(Personnage joueur) {
			int compteurItem = 0;
			for (ItemGame item : getItemsCheptel()) {
				if(item.getPosX() == joueur.getPosX() && item.getPosY() == joueur.getPosY()) {
					compteurItem++;
				}
			}
			return compteurItem;
		}
		
		


	// M�thode servant a int�grer les monstres et les objets dans l'affichage
		public boolean displayItemsMonster(int x, int y, Donjon donjon) {
			boolean isMonster = false;
			int compteurMonster = 0;
			int compteurItem = 0;
			for (Monstre monster : getMonsterCheptel()) {
				if(monster.getPosX() == x && monster.getPosY() == y) {
					isMonster = true;
					compteurMonster++;
				}
			}
			for (ItemGame item : getItemsCheptel()) {
				if(item.getPosX() == x && item.getPosY() == y) {
					isMonster = true;
					compteurItem++;
				}
			}
			
			if (compteurMonster > 0 && compteurItem == 0) {
				if (compteurMonster > 1) {
					if(donjon.getDonjon()[x][y].isdW()) {
						System.out.print("|"+compteurMonster+"M ");
					} else {
						System.out.print(" "+compteurMonster+"M ");
					}
				} else {
					if(donjon.getDonjon()[x][y].isdW()) {
						System.out.print("| "+"M ");
					} else {
						System.out.print(" "+" M ");
					}
				}
			}
			if (compteurMonster > 0 && compteurItem > 0) {
				if (compteurMonster > 1) {
					if(donjon.getDonjon()[x][y].isdW()) {
						System.out.print("|"+compteurMonster+"Mo");
					} else {
						System.out.print(" "+compteurMonster+"Mo");
					}
				} else {
					if(donjon.getDonjon()[x][y].isdW()) {
						System.out.print("| "+"Mo");
					} else {
						System.out.print(" "+" Mo");
					}
				}
			
			}
			
			if (compteurMonster == 0 && compteurItem > 0) {
				if (compteurItem > 1) {
					if(donjon.getDonjon()[x][y].isdW()) {
						System.out.print("|"+compteurItem+"o ");
					} else {
						System.out.print(" "+compteurItem+"o ");
					}
				} else {
					if(donjon.getDonjon()[x][y].isdW()) {
						System.out.print("| "+"o ");
					} else {
						System.out.print(" "+" o ");
					}
				}
			}
			return isMonster;
		}
	 // ---------------------------------------METHODES USUELLES

	 // Permet de r�cup�rer l'id de la chambre o� se situe le personnage
	public int getRoomId(Personnage perso, Donjon donjon) {

		 int roomId = 0;
		 
		 // On r�cup�re l'id de la chambre o� se trouve le perso
		 for (int i = 0; i < donjon.getX(); i++ ) {
			 for (int j = 0; j < donjon.getY(); j++) {
				 if (donjon.getDonjon()[i][j].getPositionX() == perso.getPosX() && donjon.getDonjon()[i][j].getPositionY() == perso.getPosY() ) {
					 roomId = donjon.getDonjon()[i][j].getId();
					 i = donjon.getX();
					 j = donjon.getY();
				 }
			 }

		 }
		 
		 return roomId;
	}
	
	

	 
	public static int randInt(int min, int max) {
	    Random Rand = new Random();
	    return Rand.nextInt((max - min) + 1) + min;
	}
}
