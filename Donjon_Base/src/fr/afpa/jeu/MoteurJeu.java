package fr.afpa.jeu;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import fr.afpa.composant.*;
import fr.afpa.plateau.Donjon;
import fr.afpa.user.User;
import fr.afpa.utils.Utils;

public class MoteurJeu {
	private Donjon donjon;
	private Personnage joueur;
	private User user;
	private ItemsMonsterCheptel itemsMonsterCheptel;
	private int end;
	private int compteur;

	
	public MoteurJeu() {
		initialisation();
		letsPlay();
		finPartie();
	}
	
	

	
	
//-------------------------------------------------------------MOTEUR DU JEU----------------------------------------------------------------------------------------------------
	

	
	public void letsPlay() {
		
		// Verification que le joueur n'est pas � la sortie
		int x = 0; int y = 0;
		for( int i = 0; i < donjon.getX(); i++) {
			for(int j = 0; j < donjon.getY(); j++) {
				if(donjon.getDonjon()[i][j].getId() == donjon.getEnd()) {
					x = donjon.getDonjon()[i][j].getPositionX();
					y = donjon.getDonjon()[i][j].getPositionY();
					i = donjon.getX();
					break;
				}
			}
		}
		
		cicatrisation();
		menu();
		itemsMonsterCheptel.pickItemFightMonster(joueur);
		
		// Verification que le joueur n'a pas perdu son combat
		if(joueur.getPointDeVie() > 0) {
			
			donjon.checkPossibilityDisplacement(joueur);
			donjon.setUserChoice(joueur);
			itemsMonsterCheptel.moveSillyMonsters(donjon, joueur );
			itemsMonsterCheptel.moveCleverMonster( donjon, joueur );
			
			// Condition d'arret ou de r�cursivit� de la fonction letsPlay
			if (joueur.getPosX() != x || joueur.getPosY() != y) {
				compteur++;
				itemsMonsterCheptel.setCompteurCleverMonster(itemsMonsterCheptel.getCompteurCleverMonster()+1);
				letsPlay();
			} else {
				sortie();
			}
			
		}
	}
	


/*
 * METHODE MENU-----------------------------------------------------------------------------------------------------------------------------------------------------
 */
	
	 
	 public void menu() {
		 System.out.println("----------------------------------------*-----MENU-----*--------------------------------------------");
		 System.out.println("---------------------------**  1 - Souhaitez vous afficher la map ? **------------------------------"
		 			    + "\n-------------------------**  2 - Souhaitez vous afficher votre etat ? **----------------------------"
		 			 + " \n\n                         Tout autre chiffre pour retourner � l'exploration");
		 	
		System.out.println("-----------------------------------------------------------------------------------------------------");
	 
		 	Scanner in = new Scanner ( System.in);
		 	int choice = in.nextInt();
			if (choice == 1) {
			 	askDisplayTotalMap();
			} else if (choice == 2) {
				joueur.getState();
			} else {
				return;
			}

			menu();
			System.out.println("----------------------------------------------------------------------------------------------------");
	 }
	 
	 public void cicatrisation() {
			if (compteur == 4 ) {
				if(joueur.getPointDeVie() <= 20)
				System.out.println("\nVos plaies cicatrisent ! "
						+ "\n+ 4 points de vie ! "
						+ "\n");
				joueur.setPointDeVie(joueur.getPointDeVie()+compteur);
				compteur = 0;
				
			}
	 }
	 
	 public void finPartie() {
		 user.setOr(joueur.getOr());
			user.setDateJeu(Instant.now());
			Sauvegardes.saveScore(user);
	 }
	 
	 // m�thode demandant si le joueur souhaite faire l'affichage de la map
	 public void askDisplayTotalMap() {
			int compteurMonster = itemsMonsterCheptel.checkMonster(joueur);
			if (  compteurMonster == 0) {
				System.out.println("\nSouhaitez-vous afficher la map compl�te ou la map de base ? "
						+ "\n1 - pour la map basique"
						+ "\n2 - pour la map compl�te"
						+ "\nN'importe quel autre caract�re pour poursuivre l'exploration");
				Scanner in = new Scanner(System.in);
				String choice = in.next();
				if("1".equals(choice)) {
					donjon.displayDonjonGamer();
				} else if("2".equals(choice)) {
					donjon.displayDonjonGamerItemsMonster(itemsMonsterCheptel);
				}
			}

	 }

	  
	 public void initialisation() {
			user  = Utils.userInfo();
			System.out.println("\n---------------------->Bienvenue dans le donjon ! Houuu ca fait peur....<---------------------");
			donjon = new Donjon();
			joueur = new Joueur(user.getPrenom());
			itemsMonsterCheptel = new ItemsMonsterCheptel(8, 6, 6, 4);
			end = donjon.getEnd();
			Utils.sleep();
			System.out.println("-----------------------------------------DEBUT DU JEU--------------------------------------------");
			System.out.println("\n                               Vous �tes � l'entr�e du Donjon.");
		    Utils.sleep();
			System.out.println("\nPressez la touche Q pour aller � gauche, Z pour monter, D pour tourner � droite et S pour descendre\n");
	 }
	 
	 public void sortie() {
		 System.out.println("***************************** PIN PIN PIN POOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO ********************************"
					+ "\nVous avez trouv� la sortie !"
					+ "\nVous �tes accueilli en grand vainqueur chez vous malgr� votre oeil en moins et votre dentition quelque peu diminu�e");
	 }

	 

}
