package fr.afpa.composant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BanditManchot extends ItemGame implements Item {

	private PotionVie potionVie;
	private PotionForce potionForce;
	private BourseOr bourseOr;
	List<ItemGame> itemsList;
	private int montantRacket;
	
	
	public BanditManchot(int posX, int posY) {
		super(posX, posY);
		itemsList = new ArrayList<>();
		potionVie = new PotionVie();
		potionForce = new PotionForce();
		bourseOr = new BourseOr();
		Collections.addAll(itemsList, potionVie, potionForce, bourseOr);
		Collections.shuffle(itemsList);
		montantRacket = Integer.parseInt(rb.getString("item.bm.or"));
	}


	public PotionVie getPotionVie() {
		return potionVie;
	}


	public void setPotionVie(PotionVie potionVie) {
		this.potionVie = potionVie;
	}


	public PotionForce getPotionForce() {
		return potionForce;
	}


	public void setPotionForce(PotionForce potionForce) {
		this.potionForce = potionForce;
	}


	public BourseOr getBourseOr() {
		return bourseOr;
	}


	public void setBourseOr(BourseOr bourseOr) {
		this.bourseOr = bourseOr;
	}


	public int getMontantRacket() {
		return montantRacket;
	}


	public void setMontantRacket(int montantRacket) {
		this.montantRacket = montantRacket;
	}
	
	public Object generateItem() {
		return itemsList.get(0);
	}
	
	
	
	
}
