package fr.afpa.composant;

import java.util.List;
import java.util.ResourceBundle;

import fr.afpa.jeu.Jeu;
import fr.afpa.jeu.MoteurJeu;
import fr.afpa.plateau.Donjon;
import fr.afpa.utils.Utils;

public class Monstre extends Personnage{

	private int posX;
	private int posY;


	public Monstre (String nom, int pointDeVie, int pointDeForce, int or) {
		super(nom, pointDeVie, pointDeForce, or);
	}
	
	public Monstre (int posX, int posY) {
		super(ItemsMonsterCheptel.randInt(Integer.parseInt(rb.getString("monstre.pdv.min")), Integer.parseInt(rb.getString("monstre.pdv.max"))) ,ItemsMonsterCheptel.randInt(Integer.parseInt(rb.getString("monstre.pdf.min")), Integer.parseInt(rb.getString("monstre.pdf.max"))), ItemsMonsterCheptel.randInt(Integer.parseInt(rb.getString("monstre.or.min")), Integer.parseInt(rb.getString("monstre.or.max"))) );
		this.posX = posX;
		this.posY = posY;
	}
	
	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}
	
	

	@Override
	public String toString() {
		return "\nOLALALA"
				+ "\n\nVous faites maintenant face � " + getNom()+ ", Monstre reconnu de son �tat, "+ItemsMonsterCheptel.randInt(2, 35)+" ans de m�tier." 
				+"\n --> points de vie : " + pointDeVie + ", points de force : "+ pointDeForce + ", l'�tat du porte-monnaie : " + or+" pi�ces d'or." 
				+"\n\n"+ getNom()+ " n'est pas content et il compte bien vous le montrer � l'aide de sa planche � clous" ;
	}

	public void getPositions() {
		System.out.println("x : "+posX+", y :"+posY);
	}
	
	public void fight(Personnage joueur) {
		joueur.setPointDeVie(joueur.getPointDeVie()-getPointDeForce());
	}
	
	
	
	 public void moveMonster(Personnage monster, Donjon donjon) {
		 int x = getPosX();
		 int y = getPosY();
		 System.out.println("position du monstre x : "+x+", y :"+y);
		if(donjon.checkDown(monster)) {
			setPosY(y+1);
			System.out.println("un monstre a boug� en bas !");
		} else 	if(donjon.checkUp(monster)) {
			setPosY(y-1);
			System.out.println("un monstre a boug� en haut !");
		} else if(donjon.checkRight(monster)) {
			setPosX(x+1);
			System.out.println("un monstre a boug� a droite !");
		} else if(donjon.checkLeft(monster)) {
			setPosX(x-1);
			System.out.println("un monstre a boug� a gauche !");
		}
		
		
	 }


}
