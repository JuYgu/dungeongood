# README #


### Lot° 2 ###

### Démarche suivie pour la réalisation du projet ###


* En premier lieu, défrichage du web pour trouver tout ce qui concernait de près ou de loin la génération d'un labyrinthe.

* Après avoir étudié un certain nombre de documents, il est apparu que le niveau requis pour créer soit même ce code était beaucoup trop élevé ( même en y mettant une dose importante de bonne volonté ). La démarche s'est donc orientée dans la recherche d'un code existant qu'il faudrait tester et analyser, facilitant ainsi la compréhension globale de la génération de notre labyrinthe.

* Nous avons donc travaillé à partir de ce code :
------> https://rosettacode.org/wiki/Maze_generation#Java

    * une analyse a été réalisée d'abord pour comprendre la sémantique du code ( les énumérations, les opérateurs bitWise, les fonctions de Collections...)
    * puis une analyse du fonctionnement ( plétores de syso et manipulation du code )
    -> nous avons décidé de garder les méthodes et "log" que nous avons créés lors de l'étape de compréhension. Elles sont trouvables dans la classe Donjon.

* Un fois le code modifié et adapté à nos contraintes, nous avons été confronté à un autre point bloquant : la recherche du chemin le plus long. Ayant plus de clés de compréhension, nous avons donc repris la base de nos recherches( une liste partielle des sources est disponible à la fin du document.)

* Pour la suite de la création des classes et méthodes du jeu (concernant le lot n°1), le processus a été beaucoup plus fluide. Il apparait cependant qu'il reste des points à éclaircir ou à revoir (les interfaces et classes abstraites notamment) afin d'optimiser et d'assainir le code.


### Configuration ###

* Une sauvegarde du joueur est réalisée à la fin de chaque partie. Vous devez parametrer le chemin de la sauvegarde dans le fichier config.properties ( package properties), occurence "sauvegarde.fichier".  

### Sources ###
 
 * Ci-dessous, une liste non-exaustive de liens nous ayant aidé sur ce projet :
 
  #Lot°1#
 - https://www.codesdope.com/blog/article/backtracking-to-solve-a-rat-in-a-maze-c-java-pytho/
 - http://www.shionn.org/algorihtme-donjon-aleatoire
 - https://www.lama.univ-savoie.fr/mediawiki/index.php/G%C3%A9n%C3%A9ration_et_r%C3%A9solution_de_labyrinthes_II
 - https://www.codeflow.site/fr/article/java-solve-maze
 - https://www.dcode.fr/generateur-labyrinthe
 - https://web.maths.unsw.edu.au/~lafaye/CCM/java/javaop.htm
 -http://www.enseignement.polytechnique.fr/informatique/INF411/cc/cc-2015-corrige.pdf
 - https://www.youtube.com/watch?v=qyFbX5xoF4Y&
 - https://www.youtube.com/watch?v=Y37-gB83HKE
 - https://www.youtube.com/watch?v=nhKU5IbJBEg
 

