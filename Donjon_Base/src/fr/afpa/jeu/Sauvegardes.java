package fr.afpa.jeu;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fr.afpa.user.User;
import fr.afpa.user.User;

import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeMap;

public class Sauvegardes {
	
	static ResourceBundle rb = ResourceBundle.getBundle("fr.afpa.properties.config");
	static String fichierSauvegarde;
	static File fichier;
	
	public static void saveScore(User joueur) {
		
		String fichierSauvegarde = rb.getString("sauvegarde.fichier");
		ObjectOutputStream oos = null;
		fichier = new File(fichierSauvegarde);

		
		try {
			fichier.createNewFile();

		} catch (IOException e1) {
			e1.printStackTrace();
		}
			
	    // Ecriture en utilisant la serialisation
		try {
			FileOutputStream f = new FileOutputStream(fichier, true);
			oos = new ObjectOutputStream(f);
			oos.writeObject(joueur);

		} catch (Exception e) {
			System.out.println("Erreur " + e);	
		} finally {
			try {
				oos.close();
				oos.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	
	}
	
	public static void getScore() {
		
	List<User> histoGamers = new ArrayList< User>();
	String fichierSauvegarde = rb.getString("sauvegarde.fichier");
		// Lecture en utilisant la serialisation
	try {
			FileInputStream f = new FileInputStream(fichierSauvegarde);
			ObjectInputStream ios = null;
			User newJoueur = null;
			while(true) {
				try {
					ios = new ObjectInputStream(f);
					newJoueur = (User) ios.readObject();
					histoGamers.add(newJoueur);
					System.out.println();
				} catch(Exception ex){
					break;
				} finally {
					
				}
				
			}
			
			Collections.sort(histoGamers, (a,b) -> b.compareTo(a));
			
		      if(histoGamers.size() == 0) {
		    	  System.out.println("Il n'y a pas encore de sauvegardes !");
		      } else {
					System.out.println("Voici les champions par ordre croissant !\n");
					
				      for (int i = 0; i <= 10; i++) {
				    	  LocalDate localDate = LocalDateTime.ofInstant(histoGamers.get(i).getDateJeu(), ZoneOffset.UTC).toLocalDate();
				    	 System.out.println(histoGamers.get(i).getPrenom()+" "+histoGamers.get(i).getNom()+" : "+histoGamers.get(i).getOr()+" pi�ces d'or. Date de jeu : "+localDate);
				      }
		      }
			
			ios.close();		

	} catch (Exception e) {

	}
	
	}
}
