package fr.afpa.composant;

public class PotionForce extends ItemGame implements Item {

	int pointDeForce;
	
	public PotionForce(int posX, int posY) {
		super(posX, posY);
		pointDeForce = Integer.parseInt(rb.getString("item.pdf"));
	}
	
	public PotionForce() {
		pointDeForce = Integer.parseInt(rb.getString("item.pdf"));
	}

	public int getPointDeForce() {
		return pointDeForce;
	}

	public void setPointDeForce(int pointDeForce) {
		this.pointDeForce = pointDeForce;
	}
	


}
