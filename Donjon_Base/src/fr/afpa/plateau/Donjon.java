package fr.afpa.plateau;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import java.util.LinkedList;
import java.util.Map;


import fr.afpa.composant.ItemsMonsterCheptel;
import fr.afpa.composant.Joueur;
import fr.afpa.composant.Personnage;


import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.Set;


public class Donjon {
	
	private int x;
	private int y;
	private Salle [][] donjonPlateau;
	private final int[][] maze;
	private LinkedList <Salle> listSalles;
	private int end;
	static ResourceBundle rb = ResourceBundle.getBundle("fr.afpa.properties.config");
	
	public Donjon() {
		Salle.setCount(0);
		this.x = Integer.parseInt(rb.getString("donjon.x"));
		this.y = Integer.parseInt(rb.getString("donjon.y"));
		donjonPlateau = new Salle [this.x][this.y];
		maze = new int[this.x][this.y];
		end = this.x*this.y;
		generateAndCheck(0,0);
	}
	
	public Salle[][] getDonjon() {
		return donjonPlateau;
	}

	public void setDonjon(Salle[][] donjon) {
		this.donjonPlateau = donjon;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	
	public void displayEnd() {
		System.out.println("La sortie est a l'id "+this.end);
	}

	/*
	 * PARTIE DE GENERATION DU DONJON ---------------------------------------------------------------------------------------------------------------------------------------
	 */
	
	// G�n�ration du labyrinthe
	public void generateMaze(int cx, int cy) {
		DIR[] dirs = DIR.values();

		Collections.shuffle(Arrays.asList(dirs));
		
		for (DIR dir : dirs) {
		
			int nx = cx + dir.dx;
			int ny = cy + dir.dy;

			
			if (between(nx, x) && between(ny, y)
					&& (donjonPlateau[nx][ny] == null)) {
	        
				if ( donjonPlateau[cx][cy] == null) {
					donjonPlateau[cx][cy] = new Salle(dir.bit, cx, cy);
					 addDoorOrEmptyDoor(cx,cy);
				}else {
					 donjonPlateau[cx][cy].setBit(donjonPlateau[cx][cy].getBit()+dir.bit);
					 addDoorOrEmptyDoor(cx,cy);
				}

				if(donjonPlateau[nx][ny] == null) {
					donjonPlateau[nx][ny] = new Salle(dir.opposite.bit, nx, ny);
					 addDoorOrEmptyDoor(nx,ny);
				} else {
					donjonPlateau[nx][ny].setBit(donjonPlateau[nx][ny].getBit()+ dir.opposite.bit);
					 addDoorOrEmptyDoor(nx,ny);
				}
				generateMaze(nx, ny);
			}
		}
	}

	// fonction r�cursive servant a reg�nerer un tableau si la sortie n'est pas positionn�e sur un egde du donjon
	public void generateAndCheck(int cx, int cy) {
		generateMaze(cx,cy);

		// initialisation et �changeur de valeur de la salle entree
		Salle entree = new SalleEntree();
		entree.setdW(donjonPlateau[0][0].isdW());
		entree.setdE(donjonPlateau[0][0].isdE());
		entree.setdN(donjonPlateau[0][0].isdN());
		entree.setdS(donjonPlateau[0][0].isdS());
		entree.setId(1);
		displaySalleBit();
		donjonPlateau[0][0] = entree;
		if (checkLastRoom()) {
			resetVisited();
			return;
		} else {
			this.donjonPlateau = new Salle [this.x][this.y];
			Salle.setCount(0);
			this.end = this.x*this.y;
			generateAndCheck(cx, cy);
		}
	}
	
	public void findLongestPath() {
		
		int coordX = 0;
		int coordY = 0;
		Set <Salle> compteurSalles = new HashSet<>();
		listSalles = new LinkedList<>();
		Map <Integer, Integer> nodes = new HashMap<>();
		listSalles.push(donjonPlateau[coordX][coordY]);
//		System.out.println("coordX :"+coordX+", coordY :"+coordY);
//		System.out.println("id :"+donjonPlateau[coordX][coordY].getId());
		int indice = 1;
		indice++;
		while((compteurSalles.size() != this.x*this.y-1)) {
			do {
//				System.out.println("x : "+coordX+",  y : "+coordY);
//				System.out.println("piece de fin"+end);
//				System.out.println(donjonPlateau[coordX][coordY].isVisited());
//				System.out.println("indice : "+indice);
				//down
				if(coordY + 1 < this.y  && donjonPlateau[coordX][coordY+1].isVisited() == false && donjonPlateau[coordX][coordY+1].getId() == indice ) {
					listSalles.push(donjonPlateau[coordX][coordY+1]);
					compteurSalles.add(donjonPlateau[coordX][coordY+1]);
					//System.out.println("down");
					donjonPlateau[coordX][coordY+1].setVisited(true);
					coordY++;
					indice++;

				} 
				// rgith
				else if (coordX + 1 < this.x  && donjonPlateau[coordX+1][coordY].isVisited() == false  && donjonPlateau[coordX+1][coordY].getId() == indice) {
					coordX++;
					listSalles.push(donjonPlateau[coordX][coordY]);
					compteurSalles.add(donjonPlateau[coordX][coordY]);
					//System.out.println("right");
					donjonPlateau[coordX][coordY].setVisited(true);
					indice++;

				}
				// up
				else if (coordY - 1 >= 0  && donjonPlateau[coordX][coordY-1].isVisited() == false  && donjonPlateau[coordX][coordY-1].getId() == indice) {
					coordY--;
					listSalles.push(donjonPlateau[coordX][coordY]);
					compteurSalles.add(donjonPlateau[coordX][coordY]);
					//System.out.println("up");
					donjonPlateau[coordX][coordY].setVisited(true);
					indice++;

				}		
				// left
				else if (coordX - 1 >= 0  && donjonPlateau[coordX-1][coordY].isVisited() == false  && donjonPlateau[coordX-1][coordY].getId() == indice) {
					coordX--;
					listSalles.push(donjonPlateau[coordX][coordY]);
					compteurSalles.add(donjonPlateau[coordX][coordY]);
					//System.out.println("left");
					donjonPlateau[coordX][coordY].setVisited(true);
					indice++;

				} else {
					
						nodes.put(listSalles.size()+1, indice-1);
						break;
				}
				
			} while (compteurSalles.size() != this.x*this.y-1);
			System.out.println(listSalles.size());
			if (!listSalles.isEmpty() ) {
				listSalles.removeFirst();
				coordX =listSalles.getFirst().getPositionX();
				coordY = listSalles.getFirst().getPositionY();
				
			}
		}
		nodes.put(listSalles.size()+1,indice-1);
		//System.out.println("sortie de boucle");
		int maxValue = Integer.MIN_VALUE;
		
		int endIndice=0;
		for( Map.Entry<Integer, Integer> entry : nodes.entrySet()) {
			if(entry.getKey() >= maxValue) {
				//System.out.println("bouble maxValue");
				maxValue =  entry.getKey();
				endIndice = entry.getValue();
			}
		}


		this.end = endIndice;
		//System.out.println(this.end);
	}
	
	
	private static boolean between(int v, int upper) {
		return (v >= 0) && (v < upper);
	}
  
	private enum DIR {
		N(1, 0, -1), S(2, 0, 1), E(4, 1, 0), W(8, -1, 0);
		private final int bit;
		private final int dx;
		private final int dy;
		private DIR opposite;
 
		// use the static initializer to resolve forward references
		static {
			N.opposite = S;
			S.opposite = N;
			E.opposite = W;
			W.opposite = E;
		}
		
		private DIR(int bit, int dx, int dy) {
			this.bit = bit;
			this.dx = dx;
			this.dy = dy;
		}
	};
	
	// m�thode servant a d�finir les portes
	public void addDoorOrEmptyDoor(int x, int y) {
		 if((donjonPlateau[x][y].getBit() & 1) == 0){
			 donjonPlateau[x][y].setDoorN("+---"); 
			 donjonPlateau[x][y].setdN(true); 
		 }else {
			 donjonPlateau[x][y].setDoorN("+   ");
			 donjonPlateau[x][y].setdN(false); 
		 }
		 
		 if ((donjonPlateau[x][y].getBit() & 2) == 0) {
			 donjonPlateau[x][y].setDoorS("+---"); 
			 donjonPlateau[x][y].setdS(true); 
		 }else {
			 donjonPlateau[x][y].setDoorS("p    ");
			 donjonPlateau[x][y].setdS(false); 
		 }
		 
		 if((donjonPlateau[x][y].getBit() & 8) == 0){
			 donjonPlateau[x][y].setDoorW("|   "); 
			 donjonPlateau[x][y].setdW(true); 
		 }else {
			 donjonPlateau[x][y].setDoorW("    ");
			 donjonPlateau[x][y].setdW(false); 
		 }
		 
		 if((donjonPlateau[x][y].getBit() & 4) == 0){
			 donjonPlateau[x][y].setDoorE("|"); 
			 donjonPlateau[x][y].setdE(true); 
		 }else {
			 donjonPlateau[x][y].setDoorE(" ");
			 donjonPlateau[x][y].setdE(false); 
		 }
	}
	
	// M�thode servant a refaire le tableau si le plus long chemin n'est pas coll� aux contours du labyrinthe
	public boolean checkLastRoom() {
		findLongestPath();

		boolean check = false;
		for (int i = 0; i< x; i++ ) {
			for(int j = 0; j< y; j++) {
				if(donjonPlateau[i][j].getId() == this.end) {
					if(	  (donjonPlateau[i][j].getPositionX() == 0 && donjonPlateau[i][j].getPositionX() < this.x)
					   || (donjonPlateau[i][j].getPositionY() == 0 && donjonPlateau[i][j].getPositionY() < this.y) ) {
						check = true;
						break;
					} else {
						check = false;
						break;
					}
				}
			}
		}
		return check;
}
	

	// r�initialise toute les chambre du donjon a visited = false
	public void resetVisited() {
		 for (int i = 0; i < this.y; i++ ) {
			 for (int j = 0; j <this.x; j++) {
				 donjonPlateau[j][i].setVisited(false);
			 }
		 }
	}
	
	
	
	
	/*
	 * PARTIE DEPLACEMENT JOUEUR DANS LE DONJON
	 */
	
	// Propose les diff�rentes possibilit�s d'avanc�es au joueur
	public  void checkPossibilityDisplacement(Personnage joueur) {
		int posX = joueur.getPosX();
		int PosY = joueur.getPosY();
		
		String possibilities = "\n\nVous pouvez vous d�placer";
		// down
		if(PosY + 1 < this.y  && getDonjon()[posX][PosY+1].isdN() == false ) {
			possibilities += " en Bas (S)";
		} 
		// right
		if(posX + 1 < this.x  && getDonjon()[posX+1][PosY].isdW() == false ) {
			possibilities += " a Droite (D)";
		} 
		// up
		if( PosY -1 >= 0  && getDonjon()[posX][PosY-1].isdS() == false ) {
			possibilities += " en Haut (Z)";
		} 
		// left
		if(posX -1 >= 0 && getDonjon()[posX-1][PosY].isdE() == false ) {
			possibilities += " a Gauche (Q)";
		} 
		
		System.out.println(possibilities
						  +"\nQue Choisissez-vous ?"
						  );
	}
	
	// Methodes de v�rification du d�placement
	public  boolean checkDown(Personnage joueur) {
		if(joueur.getPosY() + 1 < this.y  && getDonjon()[joueur.getPosX()][joueur.getPosY()+1].isdN() == false ) {
			return true;
		} 
		return false;
	}
	public   boolean checkUp(Personnage joueur) {
		if( joueur.getPosY() -1 >= 0  && getDonjon()[joueur.getPosX()][joueur.getPosY()-1].isdS() == false ) {
			
			return true;
		} 
		return false;
	}
	public  boolean checkRight(Personnage joueur) {
		if(joueur.getPosX() + 1 < this.x  && getDonjon()[joueur.getPosX()+1][joueur.getPosY()].isdW() == false ) {
			return true;
		} 
		return false;
	}	
	public boolean checkLeft(Personnage joueur) {
		if(joueur.getPosX() - 1 >= 0 && getDonjon()[joueur.getPosX()-1][joueur.getPosY()].isdE() == false ){
			return true;
		} 
		return false;
	}
	
	//D�place le joueur en fonction de la r�ponse de l'utilisateur
	public  void setUserChoice(Personnage joueur){
		Scanner in = new Scanner(System.in);
		String choice = in.next();
		if (choice.equalsIgnoreCase("Q")) {
			if(checkLeft(joueur)) {
				System.out.println("Allons dans la salle de gauche !");
				GamerDisplacement("x", -1, joueur );
			} else {
				temeraire();
				setUserChoice(joueur);
			}
		} else if (choice.equalsIgnoreCase("Z")) {
			if(checkUp(joueur)) {
				System.out.println("Allons dans la salle d'en haut !");
				GamerDisplacement("y", -1, joueur );
			} else {
				temeraire();
				setUserChoice(joueur);
			}
		} else if (choice.equalsIgnoreCase("D")) {
			if(checkRight(joueur)) {
				System.out.println("Allons dans la salle de droite !");
				GamerDisplacement("x", 1, joueur );
			} else {
				temeraire();
				setUserChoice(joueur);
			}
		} else if (choice.equalsIgnoreCase("S")) {
			if(checkDown(joueur)) {
				System.out.println("Allons dans la salle du bas !");
				GamerDisplacement("y", 1, joueur );
			} else {
				temeraire();
				setUserChoice(joueur);
			}
		} else {
			System.out.println("Merci de taper la bonne direction");
			setUserChoice(joueur);
		}
	}
	
	//Methode servant a d�placer le joueur dans la bonne salle
	public void GamerDisplacement(String coord, int direction, Personnage joueur ) {
		// suppression de la position du joueur dans la map
		for (int i = 0; i < this.x; i++) {
			for(int j = 0; j < this.y; j++) {
				if((getDonjon()[i][j].getPositionY() == joueur.getPosY()) && (getDonjon()[i][j].getPositionX() == joueur.getPosX())){
					if(getDonjon()[i][j].isdW() == true) {
						getDonjon()[i][j].setDoorW("|   ");
					} else {
						getDonjon()[i][j].setDoorW("    ");
					}
					break;
				}
			}
		}
		// D�placement du joueur dans la map et chamgement de ses coordonn�es
		if ("x".equals(coord)) {
			// A droite
			if(direction == 1) {
				for (int i = 0; i < this.x; i++) {
					for(int j = 0; j < this.y; j++) {
						if((getDonjon()[i][j].getPositionY() == joueur.getPosY()) && (getDonjon()[i][j].getPositionX() == joueur.getPosX()+1)){
							if(getDonjon()[i][j].isdW() == true) {
								getDonjon()[i][j].setDoorW("| J ");
							} else {
								getDonjon()[i][j].setDoorW("  J ");
							}
							((Joueur) joueur).seDeplacer(joueur.getPosX()+1, joueur.getPosY());
							i = this.x;
							break;
						}
						
					}
					
				}
			// A gauche
			}else if(direction == -1) {
				for (int i = 0; i < this.x; i++) {
					for(int j = 0; j < this.y; j++) {
						if((getDonjon()[i][j].getPositionY() == joueur.getPosY()) && (getDonjon()[i][j].getPositionX() == joueur.getPosX()-1)){
							if(getDonjon()[i][j].isdW() == true) {
								getDonjon()[i][j].setDoorW("| J ");
							} else {
								getDonjon()[i][j].setDoorW("  J ");
							}
							joueur.seDeplacer(joueur.getPosX()-1, joueur.getPosY());
							i =  this.x;
							break;
						}
					}
				}
			}
		} else if("y".equals(coord)) {
			// en bas
			if(direction == 1) {
				for (int i = 0; i <  this.x; i++) {
					for(int j = 0; j < this.y; j++) {
						if((getDonjon()[i][j].getPositionX() == joueur.getPosX()) && (getDonjon()[i][j].getPositionY() == joueur.getPosY()+1)){
							if(getDonjon()[i][j].isdW() == true) {
								getDonjon()[i][j].setDoorW("| J ");
							} else {
								getDonjon()[i][j].setDoorW("  J ");
							}
							joueur.seDeplacer(joueur.getPosX(), joueur.getPosY()+1);
							i =  this.x;
							break;
						}
					}
				}
			// en haut
			}else if(direction == -1) {
				for (int i = 0; i <  this.x; i++) {
					for(int j = 0; j < this.y; j++) {
						if((getDonjon()[i][j].getPositionX() == joueur.getPosX()) && (getDonjon()[i][j].getPositionY() == joueur.getPosY()-1)){
							if(getDonjon()[i][j].isdW() == true) {
								getDonjon()[i][j].setDoorW("| J ");
							} else {
								getDonjon()[i][j].setDoorW("  J ");
							}
							joueur.seDeplacer(joueur.getPosX(), joueur.getPosY()-1);
							i = this.x;
							break;
						}
					}
				}
			}
		}
	}
	
// METHODE AFFICHAGE ADDITIONNEL
	
	public void temeraire() {
		System.out.println("Vous �tes un petit t�m�raire mais vous vous prenez un mur en pleine figure. Recommencez et vous perdez un point de vie. Merci de refaire votre saisie");

	}
	
	
//------------------------------------------------------------AFFICHAGE----------------------------------------------------------------------------------------------------------	
		// Affichage de la map avec le joueur
		public void displayDonjonGamer() {
			for (int i = 0; i < getY(); i++) {
				// draw the north edge
				for (int j = 0; j < getX(); j++) {
					System.out.print(getDonjon()[j][i].getDoorN());
				}
				System.out.println("+");
				// draw the west edge
				for (int j = 0; j < getX(); j++) {
					if(j == 0 && i == 0) {
						System.out.print(getDonjon()[j][i].getDoorW() );
					} else if(getDonjon()[j][i].getId() == getEnd()){
						if((getDonjon()[j][i].getBit() & 8) == 0) {
							System.out.print("|>< " );
						} else {
							System.out.print(" >< " );
						}
					}else {
						System.out.print(getDonjon()[j][i].getDoorW());
					}
				}
				System.out.println("|");
			}
			// draw the bottom line
			for (int j = 0; j < getX(); j++) {
				System.out.print("+---");
			}
			System.out.println("+");
		}
		
		// Affichage de la map avec le joueur Et les Monstres, Et les objets
			public void displayDonjonGamerItemsMonster(ItemsMonsterCheptel itemsMonsterCheptel) {
				for (int i = 0; i < getY(); i++) {
					// draw the north edge
					for (int j = 0; j < getX(); j++) {
						System.out.print(getDonjon()[j][i].getDoorN());
					}
					System.out.println("+");
					// draw the west edge
					for (int j = 0; j < getX(); j++) {
						if(j == 0 && i == 0) {
							System.out.print(getDonjon()[j][i].getDoorW() );
						} else if(getDonjon()[j][i].getId() == getEnd()){
							if((getDonjon()[j][i].getBit() & 8) == 0) {
								System.out.print("|>< " );
							} else {
								System.out.print(" >< " );
							}
						}else {
							if(!itemsMonsterCheptel.displayItemsMonster(j,i, this)) {
								System.out.print(getDonjon()[j][i].getDoorW());	
							}
						}
					}
					System.out.println("|");
				}
				// draw the bottom line
				for (int j = 0; j < getX(); j++) {
					System.out.print("+---");
				}
				System.out.println("+");
			}
		
	
	
// -----------------------------------------METHODES D'AFFICHAGE LOG--------------------------------------------------------------------------------------------------------------------	
	
	// Affichage donjon avec coordonn�es de room
		public void display() {
			for (int i = 0; i < y; i++) {
				// draw the north edge
				
				for (int j = 0; j < x; j++) {
					
					System.out.print(donjonPlateau[j][i].getDoorN());
				}
				System.out.println("+");
				// draw the west edge
				for (int j = 0; j < x; j++) {
					if(j == 0 && i == 0) {
						System.out.print((donjonPlateau[j][i].getBit() & 1) == 0 ? ">   " : "+   ");
					} else if(donjonPlateau[j][i].getId() == this.end){
						if((donjonPlateau[j][i].getBit() & 8) == 0) {
							System.out.print("|>< " );
						} else {
							System.out.print(" >< " );
						}
						
					}else {
						System.out.print(donjonPlateau[j][i].getDoorW());
		
					}
				}
				System.out.println("|");
			}
			// draw the bottom line
			for (int j = 0; j < x; j++) {
				System.out.print("+---");
			}
			System.out.println("+");
			
		}
	// Affichage donjon avec coordonn�es de room
	public void displayCoordonnee() {
		for (int i = 0; i < y; i++) {
			// draw the north edge
			
			for (int j = 0; j < x; j++) {
				
				System.out.print((donjonPlateau[j][i].getDoorN()+donjonPlateau[j][i].getPositionX()+donjonPlateau[j][i].getPositionY()));
			}
			System.out.println("+");
			// draw the west edge
			for (int j = 0; j < x; j++) {
				if(j == 0 && i == 0) {
					System.out.print((donjonPlateau[j][i].getBit() & 1) == 0 ? ">   "+donjonPlateau[j][i].getPositionX()+donjonPlateau[j][i].getPositionY() : "+   ");
				} else if(donjonPlateau[j][i].getId() == this.end){
					if((donjonPlateau[j][i].getBit() & 8) == 0) {
						System.out.print("|>< " );
					} else {
						System.out.print(" >< " );
					}
					
				}else {
					System.out.print((donjonPlateau[j][i].getDoorW())/*)+donjon[j][i].getId()*/+donjonPlateau[j][i].getPositionX()+donjonPlateau[j][i].getPositionY());
	
				}
			}
			System.out.println("|");
		}
		// draw the bottom line
		for (int j = 0; j < x; j++) {
			System.out.print("+---");
		}
		System.out.println("+");
		
	}
	
	
	// Affichage donjon avec toutes les portes
	public void displayDoors() {
		for (int i = 0; i < y; i++) {
			// draw the north edge
			
			for (int j = 0; j < x; j++) {
				
				System.out.print(donjonPlateau[j][i].getDoorN());
			}
			System.out.println("+");
			// draw the west edge
			for (int j = 0; j < x; j++) {
				if(j == 0 && i == 0) {
					System.out.print((donjonPlateau[j][i].getBit() & 1) == 0 ? ">   " : "+   ");
				} else if(donjonPlateau[j][i].getId() == this.end){
					if((donjonPlateau[j][i].getBit() & 8) == 0) {
						System.out.print("|>< " );
					} else {
						System.out.print(" >< " );
					}
					
				}else {
					System.out.print(donjonPlateau[j][i].getDoorW());
					System.out.print(donjonPlateau[j][i].getDoorE());
	
				}
			}
			System.out.println("L");
			for (int j = 0; j < x; j++) {
				
				System.out.print(donjonPlateau[j][i].getDoorS());
			}
			System.out.println("+");
		}
		// draw the bottom line
		for (int j = 0; j < x; j++) {
			System.out.print("+---");
		}
		System.out.println("+");
		
	}
	// affichage tableau de int
	public void displayValeurBit() {
		for (int i = 0; i < y; i++) {
			// draw the north edge
			for (int j = 0; j < x; j++) {
				System.out.print((donjonPlateau[j][i].getBit() & 1) == 0 ? "+"+(donjonPlateau[j][i].getBit() & 1)+"--" : "+"+(donjonPlateau[j][i].getBit() & 1)+"  ");
			}
			System.out.println("+");
			// draw the west edge
			for (int j = 0; j < x; j++) {
				System.out.print((donjonPlateau[j][i].getBit() & 8) == 0 ? "|"+(donjonPlateau[j][i].getBit() & 8)+"  " : ""+(donjonPlateau[j][i].getBit() & 8)+"   ");
			}
			System.out.println("|");
		}
		// draw the bottom line
		for (int j = 0; j < x; j++) {
			System.out.print("+---");
		}
		System.out.println("+");
	}
	
	
	public void displaySalleBit() {
		for (int i = 0; i < y; i++) {
			// draw the north edge
			for (int j = 0; j < x; j++) {
				System.out.print("+ "+donjonPlateau[j][i].getId());
			}
			System.out.println("+");
			// draw the west edge
		}
	}
	
	public void displayMazeBit() {
		for (int i = 0; i < x; i++) {
			// draw the north edge
			for (int j = 0; j < y; j++) {
				System.out.print("+ "+maze[i][j]);
			}
			System.out.println("+");
			// draw the west edge
		}
	}
	
	// ----------------------------------------- Methode Main utilis�e pour la compr�hension et la cr�ation du labyrinthe-------------------------------------------------
//	public static void main(String[] args) {
//		int x = args.length >= 1 ? (Integer.parseInt(args[0])) : 8;
//		int y = args.length == 2 ? (Integer.parseInt(args[1])) : 8;
//		Donjon donjon = new Donjon();
//		donjon.generateMaze(0, 0);
//		//donjon.generateAndCheck();
//		donjon.display();
//		donjon.displayValeurBit();
//		donjon.displaySalleBit();
//	}
}
