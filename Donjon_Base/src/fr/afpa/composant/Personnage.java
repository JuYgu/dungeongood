package fr.afpa.composant;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

import fr.afpa.utils.Utils;

public abstract class Personnage implements Serializable, Comparable<Personnage>,Comparator<Personnage> {
	
	private transient int id;
	private String nom;
	protected int pointDeVie;
	protected int pointDeForce;
	protected int or;
	private transient int posX;
	private transient int posY;
	private Instant dateJeu;
	
	
	static int count;
	static ResourceBundle rb = ResourceBundle.getBundle("fr.afpa.properties.config");
	static String [] defaultName;
	
	
	public Personnage(String nom, int pointDeVie, int pointDeForce, int or) {
		this.id = ++count;
		this.nom = nom;
		this.pointDeVie = pointDeVie;
		this.pointDeForce = pointDeForce;
		this.or = or;
		
	}
	
	public Personnage(int pointDeVie, int pointDeForce, int or) {
		
		this.id = ++count;
		this.pointDeVie = pointDeVie;
		this.pointDeForce = pointDeForce;
		this.or = or;
		List <String> names = new ArrayList<>(Arrays.asList(defaultName));
		do {
			Collections.shuffle(names);
			System.out.println("dans la boucle shuffle");
		} while(names.get(0)== null);
		String name = names.get(0);
		this.nom = name;
		names.remove(0);
		defaultName = names.toArray(defaultName);
		
	}
	
	public Personnage() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPointDeVie() {
		return pointDeVie;
	}

	public void setPointDeVie(int pointDeVie) {
		this.pointDeVie = pointDeVie;
	}

	public int getPointDeForce() {
		return pointDeForce;
	}

	public void setPointDeForce(int pointDeForce) {
		this.pointDeForce = pointDeForce;
	}

	public int getOr() {
		return or;
	}

	public void setOr(int or) {
		this.or = or;
	}
	
	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public Instant getDateJeu() {
		return dateJeu;
	}

	public void setDateJeu(Instant dateJeu) {
		this.dateJeu = dateJeu;
	}

	public void seDeplacer(int x, int y) {
		setPosX(x);
		setPosY(y);
	}
	
	public void fight(Personnage personnage) {
	}


	public int compareTo(Personnage joueur) {
		
		 return (this.or - joueur.or );
	}
	
	public int compare (Personnage a, Personnage b) {
		return a.getOr() - b.getOr();
		}

	public void getState() {
		System.out.println( getNom()+"\n Points de vie :" + pointDeVie + ", Points de force :" + pointDeForce + ", or :" + or+" pi�ces d'or");
		if (this.pointDeVie < 10) {
			System.out.println("Houlala, vous �tes mal en point ! Pensez a marcher un peu loin des monstres, vous pourrez cicatriser");
		}
	}
	
	
	
	 // Methode permettant d'utiliser la valeur de l'objet et de le supprimer ensuite
	 public void useItem(int itemId, List<ItemGame> list) {
		 int indice = 0;
		 for (ItemGame item : list) {
				if(item.getId() == itemId) {
					indice = list.indexOf(item);
					if( item instanceof BourseOr) {
						setOr(getOr()+((BourseOr) item).getOr());
						System.out.println("Aaaah, �a fait du bien, un peu d'or !"
								+ "\nVous avez gagn� "+((BourseOr) item).getOr()+" pi�ces d'or");
						 Utils.sleep();
					} else if (item instanceof PotionVie) {
						setPointDeVie(getPointDeVie()+((PotionVie) item).getPointDeVie());
						System.out.println("Aaaah, �a fait du bien, un peu de vie !"
								+ "\nVous avez gagn� "+((PotionVie) item).getPointDeVie()+" points de vie");
						Utils.sleep();
					} else if (item instanceof PotionForce) {
						setPointDeForce(getPointDeForce()+((PotionForce) item).getPointDeForce());
						System.out.println("Aaaah, �a fait du bien, un peu de force !"
								+ "\nVous avez gagn� "+((PotionForce) item).getPointDeForce()+" points de force");
						Utils.sleep();
					} else if ( item instanceof BanditManchot ) {
						if(getOr() >= ((BanditManchot)item).getMontantRacket()) {
							Object newItem = ((BanditManchot)item).generateItem();
							if( newItem instanceof BourseOr) {
								setOr(getOr()+((BourseOr) newItem).getOr());
								System.out.println("Aaaah, �a fait du bien, un peu d'or !"
										+ "\nVous avez gagn� "+((BourseOr)  newItem).getOr()+" pi�ces d'or");
								Utils.sleep();
							} else if (newItem instanceof PotionVie) {
								setPointDeVie(getPointDeVie()+((PotionVie)  newItem).getPointDeVie());
								System.out.println("Aaaah, �a fait du bien, un peu de vie !"
										+ "\nVous avez gagn� "+((PotionVie)  newItem).getPointDeVie()+" points de vie");
								Utils.sleep();
							} else if (newItem instanceof PotionForce) {
								setPointDeForce(getPointDeForce()+((PotionForce)  newItem).getPointDeForce());
								System.out.println("Aaaah, �a fait du bien, un peu de force !"
										+ "\nVous avez gagn� "+((PotionForce)  newItem).getPointDeForce()+" points de force");
								Utils.sleep();
							}
							setOr(getOr()-((BanditManchot)item).getMontantRacket());
						} else {
							System.out.println("Vous n'avez pas un sou en poche et vous souhaitez quand m�me jouer au casino ? Revoyez vos priorit�s !");
						}
					}
				}
			}
		 list.remove(indice); 
	 }

	 
	 
}


